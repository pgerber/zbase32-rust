# Implementation of zbase32 in Rust

[![crates.io](https://meritbadge.herokuapp.com/zbase32)](https://crates.io/crates/zbase32)

This is an implementation of the human-oriented base-32 encoding called
[zbase32](https://philzimmermann.com/docs/human-oriented-base-32-encoding.txt).

[The Documentation has some more details.](https://docs.rs/zbase32)
